package com.cxy521.constant;

/**
 * @version 1.0
 * @author： wucheng
 * @date： 2021-09-02 22:25
 */

public interface Constants {

    // 留言板审核状态： 0：初始化 ， 1：通过， 2：拒绝
    interface MessageApproveState{
        Integer INIT  = 0;
        Integer PASSED = 1;
        Integer IGNORED = 2;
    }
}
