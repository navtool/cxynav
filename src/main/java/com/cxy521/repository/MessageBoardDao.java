package com.cxy521.repository;

import com.cxy521.model.bo.MessageBoardBO;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @version 1.0
 * @author： wucheng
 * @date： 2021-09-02 19:14
 */

@Repository
public interface MessageBoardDao extends JpaRepositoryImplementation<MessageBoardBO, String> {
    List<MessageBoardBO> findByState(Integer state);

    Optional<MessageBoardBO> findById(Long id);
}
