package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: SystemEnvBO
 * @Description:
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
 */
@Table(name = "system_env")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SystemEnvBO {

    /**
     * 键
     */
    @Id
    @Column(name = "item", nullable = false, length = 30)
    private String item;

    /**
     * 值
     */
    @Column(name = "value")
    private String value;
}