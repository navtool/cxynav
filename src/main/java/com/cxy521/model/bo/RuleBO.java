package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: RuleBO
 * @Description:
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
 */
@Table(name = "rule")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class RuleBO {
    /**
     * 角色ID
     */
    @Id
    @Column(name = "rule_id", nullable = false, length = 20)
    private String ruleID;

    /**
     * 角色名
     * admin:管理员
     * admin:管理员
     */
    @Column(name = "rule_name", nullable = false, length = 20)
    private String ruleName;

    /**
     * 菜单列表
     * 多条记录以`,`隔开
     */
    @Column(name = "menu_list", length = 500)
    private String menuList;

    /**
     * 主画面
     */
    @Column(name = "main_menu_seq")
    private String mainMenuSeq;
}