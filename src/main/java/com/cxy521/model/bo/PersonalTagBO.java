package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: PersonalTagBO
 * @Description:
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
 */
@Table(name = "personal_tag")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PersonalTagBO {
    /**
     * 逻辑主键
     */
    @Id
    @Column(name = "seq", nullable = false, length = 32)
    private String seq;

    /**
     * 帐号ID
     * 对应帐号信息表seq
     */
    @Column(name = "ac_seq", nullable = false, length = 32)
    private String acSeq;

    /**
     * 链接ID
     * 对应链接信息表seq
     */
    @Column(name = "link_seq", nullable = false, length = 32)
    private String linkSeq;

    /**
     * 创建时间
     */
    @Column(name = "cr_date", nullable = false, length = 17)
    private String crDate;

    /**
     * 更新时间
     * 初始值与创建时间一致
     */
    @Column(name = "upd_date", length = 17)
    private String updDate;
}