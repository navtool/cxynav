package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: LinkCategoryBO
 * @Description:
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
 */
@Table(name = "link_category")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class LinkCategoryBO {
    /**
     * 逻辑主键
     * 使用UUID生成
     */
    @Id
    @Column(name = "seq", nullable = false, length = 32)
    private String seq;

    /**
     * 分类名称
     */
    @Column(name = "category_name", nullable = false, unique = true, length = 50)
    private String categoryName;

    /**
     * 分类排序
     * 默认值为0
     */
    @Column(name = "category_index")
    private Integer categoryIndex;

    /**
     * 创建时间
     */
    @Column(name = "cr_date", nullable = false, length = 17)
    private String crDate;

    /**
     * 更新时间
     * 初始值与创建时间一致
     */
    @Column(name = "upd_date", length = 17)
    private String updDate;
}