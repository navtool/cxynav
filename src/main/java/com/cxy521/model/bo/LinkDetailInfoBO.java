package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: LinkDetailInfoBO
 * @Description:
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
 */
@Table(name = "link_detail_info")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class LinkDetailInfoBO {
    /**
     * 逻辑主键
     * 使用UUID生成,对应链接基本信息表seq
     */
    @Id
    @Column(name = "seq", nullable = false, length = 32)
    private String seq;

    /**
     * 详细内容
     */
    @Column(name = "detail", length = 500)
    private String detail;

    /**
     * 文章url
     */
    @Column(name = "article_url", length = 500)
    private String articleUrl;

    /**
     * 创建时间
     */
    @Column(name = "cr_date", nullable = false, length = 17)
    private String crDate;

    /**
     * 更新时间
     * 初始值与创建时间一致
     */
    @Column(name = "upd_date", length = 17)
    private String updDate;
}