package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: UserBasicInfoBO
 * @Description:
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
 */
@Table(name = "user_basic_info")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserBasicInfoBO {
    /**
     * 逻辑主键
     * 使用UUID生成,对应用户帐号表seq
     */
    @Id
    @Column(name = "seq", nullable = false, length = 32)
    private String seq;

    /**
     * 手机号
     */
    @Column(name = "mobile", nullable = false)
    private String mobile;
    /**
     * 名称
     */
    @Column(name = "ac_name", nullable = false, unique = true)
    private String acName;

    /**
     * 性别
     * 0:男
     * 1:女
     * 2:无 (默认)
     */
    @Column(name = "sex")
    private char sex;

    /**
     * 生日
     */
    @Column(name = "birth", nullable = false, length = 8)
    private String birth;

    /**
     * 头像url
     * 默认为默认头像url
     */
    @Column(name = "portrait_url", nullable = false, length = 500)
    private String portraitUrl;

    /**
     * 微信号
     */
    @Column(name = "wechat", length = 50)
    private String wechat;

    /**
     * gitee号
     */
    @Column(name = "gitee", length = 50)
    private String gitee;

    /**
     * 简介
     */
    @Column(name = "introduction", length = 200)
    private String introduction;

    /**
     * 创建时间
     */
    @Column(name = "cr_date", nullable = false, length = 17)
    private String crDate;

    /**
     * 更新时间
     * 初始值与创建时间一致
     */
    @Column(name = "upd_date", length = 17)
    private String updDate;
}