package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: AccountInfoBO
 * @Description:
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
 */
@Table(name = "account_info")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AccountInfoBO {
    /**
     * 逻辑主键
     * 对应用户基本信息表seq
     */
    @Id
    @Column(name = "seq", nullable = false, length = 32)
    private String seq;

    /**
     * 帐号
     */
    @Column(name = "account", nullable = false, unique = true, length = 11)
    private String account;

    /**
     * 帐号密码
     */
    @Column(name = "ac_password", nullable = false, length = 32)
    private String acPassword;

    /**
     * 角色ID
     * 对应角色表rule_id
     */
    @Column(name = "rule_id", nullable = false, length = 20)
    private String ruleID;

    /**
     * 创建时间
     */
    @Column(name = "cr_date", nullable = false, length = 17)
    private String crDate;

    /**
     * 更新时间
     * 初始值与创建时间一致
     */
    @Column(name = "upd_date", length = 17)
    private String updDate;
}