package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: LinkBasicInfoBO
 * @Description: 
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
*/
@Table(name = "link_basic_info")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class LinkBasicInfoBO {
    /**
     * 逻辑主键
     * 使用UUID生成
     */
    @Id
    @Column(name = "seq", nullable = false, length = 32)
    private String seq;

    /**
     * 链接名称
     */
    @Column(name = "link_name", nullable = false, length = 50)
    private String linkName;

    /**
     * 分类ID
     * 对应链接分类表seq
     */
    @Column(name = "category_id", nullable = false, length = 32)
    private String categoryID;

    /**
     * 系统推荐
     * 0:否 (默认)
     * 1:是
     */
    @Column(name = "sys_rec_flg")
    private char sysRecFlg;

    /**
     * 个人推荐
     * 0:否 (默认)
     * 1:是
     */
    @Column(name = "per_rec_flg")
    private char perRecFlg;

    /**
     * 排序
     */
    @Column(name = "link_index")
    private char linkIndex;

    /**
     * 创建时间
     */
    @Column(name = "cr_date", nullable = false, length = 17)
    private String crDate;

    /**
     * 更新时间
     * 初始值与创建时间一致
     */
    @Column(name = "upd_date", length = 17)
    private String updDate;

}