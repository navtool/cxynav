package com.cxy521.model.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @version 1.0
 * @author： wucheng
 * @date： 2021-08-28 18:34
 */

@Data
@Accessors(chain = true)
@Entity()
@Table(name = "MESSAGE_BOARD")
public class MessageBoardBO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 30)
//    @TableField(value = "name")
    private String name;
    @Column(name = "web_site", length = 30)
//    @TableField(value = "web_site")
    private String webSite;
    @Column(name = "content", length = 30)
//    @TableField(value = "content")
    private String content;
    @Column(name = "state", length = 30)
//    @TableField(value = "state")
    private Integer state;
    @Column(name = "created_date", length = 30)
//    @TableField(value = "created_date")
    private LocalDateTime createdDate;
}
