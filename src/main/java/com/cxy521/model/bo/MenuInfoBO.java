package com.cxy521.model.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @ClassName: MenuInfoBO
 * @Description:
 * @Author: Solo.Lee
 * @Version: 1.0
 * @Date: 2021/11/8
 */
@Table(name = "menu_info")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MenuInfoBO {
    /**
     * 逻辑主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "seq", nullable = false)
    private Long seq;

    /**
     * 菜单ID
     */
    @Column(name = "menu_id", nullable = false, unique = true, length = 30)
    private String menuID;

    /**
     * 菜单名称
     */
    @Column(name = "menu_name", nullable = false, length = 45)
    private String menuName;

    /**
     * 菜单指向地址
     */
    @Column(name = "menu_url", length = 100)
    private String menuUrl;

    /**
     * 父节点ID
     */
    @Column(name = "parent_id", length = 30)
    private String parentID;

    /**
     * 节点
     * 0:父节点 (默认)
     * 1:子节点
     */
    @Column(name = "node")
    private char node;
}