-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: cxy521
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_info`
--

DROP TABLE IF EXISTS `account_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_info` (
  `seq` varchar(32) NOT NULL COMMENT '逻辑主键,对应用户基本信息表seq',
  `account` varchar(11) NOT NULL COMMENT '帐号',
  `ac_password` varchar(32) NOT NULL COMMENT '帐号密码',
  `rule_id` varchar(20) NOT NULL COMMENT '角色ID,对应角色表rule_id',
  `cr_date` varchar(17) NOT NULL COMMENT '创建时间,毫秒',
  `upd_date` varchar(17) DEFAULT NULL COMMENT '更新时间,毫秒,初始值与创建时间一致',
  UNIQUE KEY `uc_account_info_account` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='帐号信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_info`
--

LOCK TABLES `account_info` WRITE;
/*!40000 ALTER TABLE `account_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link_basic_info`
--

DROP TABLE IF EXISTS `link_basic_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `link_basic_info` (
  `seq` varchar(32) NOT NULL COMMENT '逻辑主键,UUID生成.',
  `link_name` varchar(50) NOT NULL COMMENT '链接名称',
  `category_id` varchar(32) NOT NULL COMMENT '分类ID,对应链接分类表seq',
  `sys_rec_flg` char(1) DEFAULT '0' COMMENT '系统推荐.0:否,1:是',
  `per_rec_flg` char(1) DEFAULT '0' COMMENT '个人推荐.0:否,1:是',
  `link_index` int DEFAULT '0' COMMENT '链接排序',
  `cr_date` varchar(17) NOT NULL COMMENT '创建时间,毫秒',
  `upd_date` varchar(17) DEFAULT '' COMMENT '更新时间,毫秒,初始值与创建时间一致'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='链接基本信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link_basic_info`
--

LOCK TABLES `link_basic_info` WRITE;
/*!40000 ALTER TABLE `link_basic_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `link_basic_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link_category`
--

DROP TABLE IF EXISTS `link_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `link_category` (
  `seq` varchar(32) NOT NULL COMMENT '逻辑主键,UUID生成.',
  `category_name` varchar(50) NOT NULL COMMENT '链接分类名称',
  `category_index` int DEFAULT '0' COMMENT '分类排序',
  `cr_date` varchar(17) NOT NULL COMMENT '创建时间,毫秒',
  `upd_date` varchar(17) DEFAULT '' COMMENT '更新时间,毫秒,初始值与创建时间一致',
  UNIQUE KEY `uc_link_category_category_name` (`category_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='链接分类表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link_category`
--

LOCK TABLES `link_category` WRITE;
/*!40000 ALTER TABLE `link_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `link_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link_detail_info`
--

DROP TABLE IF EXISTS `link_detail_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `link_detail_info` (
  `seq` varchar(32) NOT NULL COMMENT '逻辑主键,对应链接基本信息表seq',
  `detail` varchar(500) DEFAULT '' COMMENT '详细内容',
  `article_url` varchar(500) DEFAULT '' COMMENT '文章Url',
  `cr_date` varchar(17) NOT NULL COMMENT '创建时间,毫秒',
  `upd_date` varchar(17) DEFAULT '' COMMENT '更新时间,毫秒,初始值与创建时间一致'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='链接详细信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link_detail_info`
--

LOCK TABLES `link_detail_info` WRITE;
/*!40000 ALTER TABLE `link_detail_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `link_detail_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_info`
--

DROP TABLE IF EXISTS `menu_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_info` (
  `seq` bigint NOT NULL AUTO_INCREMENT COMMENT '逻辑主键',
  `menu_id` varchar(30) NOT NULL COMMENT '菜单ID,UUID生成',
  `menu_name` varchar(45) NOT NULL COMMENT '菜单名称',
  `menu_url` varchar(100) DEFAULT '' COMMENT '菜单指向地址',
  `parent_id` varchar(30) DEFAULT '' COMMENT '父节点ID',
  `node` char(1) DEFAULT '0' COMMENT '节点,0:父节点(默认),1:子节点',
  PRIMARY KEY (`seq`),
  UNIQUE KEY `uc_menu_info_menu` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单对照表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_info`
--

LOCK TABLES `menu_info` WRITE;
/*!40000 ALTER TABLE `menu_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_board`
--

DROP TABLE IF EXISTS `message_board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message_board` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `web_site` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `state` int DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_board`
--

LOCK TABLES `message_board` WRITE;
/*!40000 ALTER TABLE `message_board` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_tag`
--

DROP TABLE IF EXISTS `personal_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_tag` (
  `seq` varchar(32) NOT NULL COMMENT '逻辑主键,UUID生成',
  `ac_seq` varchar(32) NOT NULL COMMENT '帐号ID,对应帐号信息表seq',
  `link_seq` varchar(32) NOT NULL COMMENT '链接ID,对应链接信息表seq',
  `cr_date` varchar(17) NOT NULL COMMENT '创建时间,毫秒',
  `upd_date` varchar(17) DEFAULT NULL COMMENT '更新时间,毫秒,初始值与创建时间一致'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_tag`
--

LOCK TABLES `personal_tag` WRITE;
/*!40000 ALTER TABLE `personal_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule`
--

DROP TABLE IF EXISTS `rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rule` (
  `rule_id` varchar(20) NOT NULL COMMENT '角色ID,UUID生成',
  `rule_name` varchar(20) NOT NULL COMMENT '角色名,admin:管理员,admin:管理员',
  `menu_list` varchar(500) DEFAULT NULL COMMENT '菜单列表,多条记录以`,`隔开',
  `main_menu_seq` varchar(255) DEFAULT NULL COMMENT '主画面,对应菜单对照表seq',
  `cr_date` varchar(17) NOT NULL COMMENT '创建时间,毫秒',
  `upd_date` varchar(17) DEFAULT NULL COMMENT '更新时间,毫秒,初始值与创建时间一致'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule`
--

LOCK TABLES `rule` WRITE;
/*!40000 ALTER TABLE `rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_env`
--

DROP TABLE IF EXISTS `system_env`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `system_env` (
  `item` varchar(30) NOT NULL COMMENT '键',
  `value` varchar(255) DEFAULT NULL COMMENT '值',
  PRIMARY KEY (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_env`
--

LOCK TABLES `system_env` WRITE;
/*!40000 ALTER TABLE `system_env` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_env` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-08 15:13:44
